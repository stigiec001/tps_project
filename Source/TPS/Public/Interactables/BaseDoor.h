// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactables/InteractableActor.h"
#include "BaseDoor.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API ABaseDoor : public AInteractableActor
{
	GENERATED_BODY()

public:

	ABaseDoor();
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsOpen = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static Mesh")
	UStaticMesh* DoorMesh;

	
};
