// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TPSPlayerController.generated.h"

UCLASS()
class ATPSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATPSPlayerController();

	void SetInteraction(bool _bool) { bCanInteract = _bool; };

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();
	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);
	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	// My functions and variables //

	bool bRotateEnabled = false;

	bool bAimingKeyPressed = false;
	bool bRunningKeyPressed = false;
	bool bSprintingKeyPressed = false;
	bool bCrouchingKeyPressed = false;

	bool bCanInteract = false;

	UFUNCTION(BlueprintCallable)
	bool GetAimingBool() { return bAimingKeyPressed; };
	UFUNCTION(BlueprintCallable)
	bool GetCrouchingBool() { return bCrouchingKeyPressed; };

	/**	Move player using keyboard. */
	void MoveForward(float value);
	void MoveRight(float value);

	//** Aiming controls. */
	void OnAimPressed();
	void OnAimReleased();
	void RotateToCursor();

	//** Change walk mode to Run. */
	void OnRunPressed();
	void OnRunReleased();

	//** Change walk mode to Sprint. */
	void OnSprintPressed();
	void OnSprintReleased();

	//** Crouching logic. */
	void OnCrouchingPressed();

	//** Interact with objects. */
	void Interact();
};


