// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/States.h"
#include "Character/TPSCharacter.h"

void UStates::ChangeMovementState(ATPSCharacter* Character, EMovementStates NewMovementState)
{
	Character->MovementState = NewMovementState;
	Character->UpdateCharacter();
}
