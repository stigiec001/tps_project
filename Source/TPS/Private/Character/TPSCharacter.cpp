// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Controllers/TPSPlayerController.h"
#include "Interfaces/InteractableInterface.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Create and setup interaction capsule
	InteractionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("InteractionCapsule"));
	InteractionCapsule->SetupAttachment(RootComponent);
	InteractionCapsule->InitCapsuleSize(42.f, 96.f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//** Initialize variables */
	StaminaCurrent = StaminaMax;

}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Binding events
	InteractionCapsule->OnComponentBeginOverlap.AddDynamic(this, &ATPSCharacter::OnCapsuleBeginOverlap);
	InteractionCapsule->OnComponentEndOverlap.AddDynamic(this, &ATPSCharacter::OnCapsuleEndOverlap);
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void ATPSCharacter::UpdateCharacter()
{
	float NewSpeed = 0.0f;

	switch (MovementState)
	{
	case EMovementStates::Aim_State:
		NewSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementStates::Walk_State:
		NewSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementStates::Run_State:
		NewSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementStates::Sprint_State:
		NewSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

void ATPSCharacter::SpintBegin()
{
	if (bCanSprint && MovementState != EMovementStates::Sprint_State)
	{
		UStates::ChangeMovementState(this, EMovementStates::Sprint_State);
		// logic for accelaration

		GetWorld()->GetTimerManager().ClearTimer(RestoreStaminaTimerHandle);
		FTimerDelegate TimerDel = FTimerDelegate::CreateUObject(this, &ATPSCharacter::ChangeStaminaOnTimer, (StaminaConsumeRate * -1)/100);
		GetWorld()->GetTimerManager().SetTimer(ConsumeStaminaTimerHandle, TimerDel, 0.01f, true, 0.0f);
	}
}

void ATPSCharacter::SpintEnd()
{
	if (!StaminaCurrent) UStates::ChangeMovementState(this, EMovementStates::Walk_State);
	GetWorld()->GetTimerManager().ClearTimer(ConsumeStaminaTimerHandle);
	FTimerDelegate TimerDel = FTimerDelegate::CreateUObject(this, &ATPSCharacter::ChangeStaminaOnTimer, StaminaRestoreRate/100);
	GetWorld()->GetTimerManager().SetTimer(RestoreStaminaTimerHandle, TimerDel, 0.01f, true, 2.0f);
}

void ATPSCharacter::ChangeStaminaOnTimer(float Delta)
{	
	StaminaCurrent = FMath::Clamp(StaminaCurrent + Delta, 0.0f, StaminaMax);
	bCanSprint = StaminaCurrent > 0.0f ? true : false;
	if (!bCanSprint && MovementState == EMovementStates::Sprint_State) SpintEnd();
	if (StaminaCurrent == StaminaMax) GetWorld()->GetTimerManager().ClearTimer(RestoreStaminaTimerHandle);
}

void ATPSCharacter::OnCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<IInteractableInterface>(OtherActor))
	{
		InteractableObject = OtherActor;
		Cast<ATPSPlayerController>(Controller)->SetInteraction(true);
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "Begin overlapping!");
	}
}

void ATPSCharacter::OnCapsuleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Cast<IInteractableInterface>(OtherActor))
	{
		InteractableObject = nullptr;
		Cast<ATPSPlayerController>(Controller)->SetInteraction(false);
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "Out of overlapping!");
	}
}

void ATPSCharacter::Interact()
{
	if (Cast<IInteractableInterface>(InteractableObject))
	{
		Cast<IInteractableInterface>(InteractableObject)->InteractPure();
		Cast<IInteractableInterface>(InteractableObject)->Execute_Interact(InteractableObject);
	}
}

