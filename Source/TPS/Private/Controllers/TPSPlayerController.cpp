// Copyright Epic Games, Inc. All Rights Reserved.

#include "Controllers/TPSPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Character/TPSCharacter.h"
#include "Character/States.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATPSPlayerController::ATPSPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATPSPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	//if (bMoveToMouseCursor)
	//{
	//	MoveToMouseCursor();
	//}

	if (bRotateEnabled)
	{
		RotateToCursor();
	}
}

void ATPSPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	//InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATPSPlayerController::OnSetDestinationPressed);
	//InputComponent->BindAction("SetDestination", IE_Released, this, &ATPSPlayerController::OnSetDestinationReleased);

	InputComponent->BindAction("Aim", IE_Pressed, this, &ATPSPlayerController::OnAimPressed);
	InputComponent->BindAction("Aim", IE_Released, this, &ATPSPlayerController::OnAimReleased);

	InputComponent->BindAction("Run", IE_Pressed, this, &ATPSPlayerController::OnRunPressed);
	InputComponent->BindAction("Run", IE_Released, this, &ATPSPlayerController::OnRunReleased);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &ATPSPlayerController::OnSprintPressed);
	InputComponent->BindAction("Sprint", IE_Released, this, &ATPSPlayerController::OnSprintReleased);

	InputComponent->BindAction("Crouching", IE_Pressed, this, &ATPSPlayerController::OnCrouchingPressed);

	InputComponent->BindAction("Interact", IE_Pressed, this, &ATPSPlayerController::Interact);

	InputComponent->BindAxis("MoveForward", this, &ATPSPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATPSPlayerController::MoveRight);

}

void ATPSPlayerController::MoveToMouseCursor()
{
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void ATPSPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ATPSPlayerController::MoveForward(float value)
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		MyPawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), value);
	}
}

void ATPSPlayerController::MoveRight(float value)
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		MyPawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), value);
	}
}

void ATPSPlayerController::OnAimPressed()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		Cast<ATPSCharacter>(GetCharacter())->GetCharacterMovement()->bOrientRotationToMovement = false;
		UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Aim_State);
		bAimingKeyPressed = true;
		bRotateEnabled = true;
	}
}

void ATPSPlayerController::OnAimReleased()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		Cast<ATPSCharacter>(GetCharacter())->GetCharacterMovement()->bOrientRotationToMovement = true;
		if (bRunningKeyPressed)
		{
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Run_State);
		}
		else if (Cast<ATPSCharacter>(GetPawn())->MovementState != EMovementStates::Sprint_State)
		{
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Walk_State);
		}
		bAimingKeyPressed = false;
		bRotateEnabled = false;
	}
}

void ATPSPlayerController::RotateToCursor()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		float Yaw = UKismetMathLibrary::FindLookAtRotation(MyPawn->GetActorLocation(), Hit.Location).Yaw;
		MyPawn->SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}

}

void ATPSPlayerController::OnRunPressed()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		if (Cast<ATPSCharacter>(GetPawn())->bCanSprint)
		{
			Cast<ATPSCharacter>(GetCharacter())->GetCharacterMovement()->bOrientRotationToMovement = true;
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Run_State);
			bRunningKeyPressed = true;
			bRotateEnabled = false;
		}
	}
}

void ATPSPlayerController::OnRunReleased()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		if (bAimingKeyPressed)
		{
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Aim_State);
			bRotateEnabled = true;
		}
		else if (Cast<ATPSCharacter>(GetPawn())->MovementState != EMovementStates::Sprint_State)
		{
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Walk_State);
		}
		bRunningKeyPressed = false;
	}
}

void ATPSPlayerController::OnSprintPressed()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		if (Cast<ATPSCharacter>(GetPawn())->bCanSprint) 
		{
			Cast<ATPSCharacter>(GetCharacter())->GetCharacterMovement()->bOrientRotationToMovement = true;
			Cast<ATPSCharacter>(GetCharacter())->SpintBegin();
			bRotateEnabled = false;
		}
	}
}

void ATPSPlayerController::OnSprintReleased()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		if (bRunningKeyPressed)
		{
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Run_State);
		}
		else
		{
			UStates::ChangeMovementState(Cast<ATPSCharacter>(GetCharacter()), EMovementStates::Walk_State);
		}
		Cast<ATPSCharacter>(GetPawn())->SpintEnd();
	}

}

void ATPSPlayerController::OnCrouchingPressed()
{

}

void ATPSPlayerController::Interact()
{
	if (ATPSCharacter* MyPawn = Cast<ATPSCharacter>(GetPawn()))
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "Nothing to interact with!");
		if (bCanInteract)
		{
			GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "Try to interact...");
			Cast<ATPSCharacter>(GetPawn())->Interact();
		}
	}
}


void ATPSPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ATPSPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ATPSPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}
