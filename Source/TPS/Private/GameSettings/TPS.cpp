// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameSettings/TPS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS, "TPS" );

DEFINE_LOG_CATEGORY(LogTPS)
 